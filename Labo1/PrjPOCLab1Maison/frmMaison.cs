﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PrjPOCLab1Maison
{
    public partial class frmMaison : Form
    {
        private SecurityControl security;
        private SerialPort com1;
        public frmMaison()
        {
            InitializeComponent();

            this.security = new SecurityControl();

            //Activation et désactivation du système de control
            security.SysActivated += alarm_SysActivated;
            security.SysInActivated += alarm_SysInActivated;

            //Activation et désactivation de la fenetre 0
            security.F0Activated += alarm_F0Activated;
            security.F0InActivated += alarm_F0InActivated;

            //Activation et désactivation de la feenetre 1
            security.F1Activated += alarm_F1Activated;
            security.F1InActivated += alarm_F1InActivated;

            //Activation et désactivation de la fenetre 2
            security.F2Activated += alarm_F2Activated;
            security.F2InActivated += alarm_F2InActivated;

            //Activation et désactivation de la feenetre 3
            security.F3Activated += alarm_F3Activated;
            security.F3InActivated += alarm_F3InActivated;

            //Activation et désactivation de la fenetre 4
            security.F4Activated += alarm_F4Activated;
            security.F4InActivated += alarm_F4InActivated;

            //Activation et désactivation de la porte 0
            security.P0Activated += alarm_P0Activated;
            security.P0InActivated += alarm_P0InActivated;

            //Activation et désactivation de la porte 1
            security.P1Activated += alarm_P1Activated;
            security.P1InActivated += alarm_P1InActivated;

            //Activation et désactivation de la porte 2
            security.P2Activated += alarm_P2Activated;
            security.P2InActivated += alarm_P2InActivated;

            //Activation et désactivation de la porte 3
            security.P3Activated += alarm_P3Activated;
            security.P3InActivated += alarm_P3InActivated;

            //Activation et désactivation de la porte 4
            security.P4Activated += alarm_P4Activated;
            security.P4InActivated += alarm_P4InActivated;

            //Activation et désactivation de la porte 5
            security.P5Activated += alarm_P5Activated;
            security.P5InActivated += alarm_P5InActivated;

            //Activation et désactivation de la porte 6
            security.P6Activated += alarm_P6Activated;
            security.P6InActivated += alarm_P6InActivated;

            //Activation et désactivation de la porte 7
            security.P7Activated += alarm_P7Activated;
            security.P7InActivated += alarm_P7InActivated;

            //Activation et désactivation de la porte 8
            security.P8Activated += alarm_P8Activated;
            security.P8InActivated += alarm_P8InActivated;

            //Activation et désactivation du mouvement 0
            security.DM0Activated += alarm_DM0Activated;
            security.DM0InActivated += alarm_DM0InActivated;

            //Activation et désactivation du mouvement 1
            security.DM1Activated += alarm_DM1Activated;
            security.DM1InActivated += alarm_DM1InActivated;

            //Activation et désactivation de l'incendie 0
            security.DI0Activated += alarm_DI0Activated;
            security.DI0InActivated += alarm_DI0InActivated;

            //Activation et désactivation de l'incendie 1
            security.DI1Activated += alarm_DI1Activated;
            security.DI1InActivated += alarm_DI1InActivated;

            //Activation et désactivation de l'incendie 2
            security.DI2Activated += alarm_DI2Activated;
            security.DI2InActivated += alarm_DI2InActivated;
        }

        private void frmMaison_Load(object sender, EventArgs e)
        {
            string[] listComPort = SerialPort.GetPortNames();
            Array.Sort(listComPort);

            this.com1 = new SerialPort(listComPort[0], 9600, Parity.None, 8, StopBits.One);
            if (!this.com1.IsOpen)
            {
                this.com1.Open();
                StripLibelle.Text = "COM1 est ouvert";
            }
        }

        private void alarm_DI2InActivated(object sender, EventArgs e)
        {
            Incendie2.BackColor = Color.White;
            MessageBox.Show(" L'incendie 2 est non sécurisé");
            Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte(3);
                msgToSend[1] = Convert.ToByte(2);
                msgToSend[2] = Convert.ToByte(0);

                this.com1.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void alarm_DI2Activated(object sender, EventArgs e)
        {
            Incendie2.BackColor = Color.Red;
            MessageBox.Show(" L'incendie 2 est maintenant sécurisé");
            Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte(3);
                msgToSend[1] = Convert.ToByte(2);
                msgToSend[2] = Convert.ToByte(1);

                this.com1.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void alarm_DI1InActivated(object sender, EventArgs e)
        {
            Incendie1.BackColor = Color.White;
            MessageBox.Show(" L'incendie 1 est non sécurisé");
            Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte(3);
                msgToSend[1] = Convert.ToByte(1);
                msgToSend[2] = Convert.ToByte(0);

                this.com1.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void alarm_DI1Activated(object sender, EventArgs e)
        {
            Incendie1.BackColor = Color.Red;
            MessageBox.Show(" L'incendie 1 est maintenant sécurisé");
            Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte(3);
                msgToSend[1] = Convert.ToByte(1);
                msgToSend[2] = Convert.ToByte(1);

                this.com1.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void alarm_DI0InActivated(object sender, EventArgs e)
        {
            Incendie0.BackColor = Color.White;
            MessageBox.Show(" L'incendie 0 est non sécurisé");
            Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte(3);
                msgToSend[1] = Convert.ToByte(0);
                msgToSend[2] = Convert.ToByte(0);

                this.com1.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void alarm_DI0Activated(object sender, EventArgs e)
        {
            Incendie0.BackColor = Color.Red;
            MessageBox.Show(" L'incendie 0 est maintenant sécurisé");
            Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte(3);
                msgToSend[1] = Convert.ToByte(0);
                msgToSend[2] = Convert.ToByte(1);

                this.com1.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void alarm_DM1InActivated(object sender, EventArgs e)
        {
            Mouvement1.BackColor = Color.White;
            MessageBox.Show(" Le mouvement 1 est non sécurisé");
            Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte(2);
                msgToSend[1] = Convert.ToByte(1);
                msgToSend[2] = Convert.ToByte(0);

                this.com1.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void alarm_DM1Activated(object sender, EventArgs e)
        {
            Mouvement1.BackColor = Color.Red;
            MessageBox.Show(" Le mouvement 1 est maintenant sécurisé");
            Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte(2);
                msgToSend[1] = Convert.ToByte(1);
                msgToSend[2] = Convert.ToByte(1);

                this.com1.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void alarm_DM0InActivated(object sender, EventArgs e)
        {
            Mouvement0.BackColor = Color.White;
            MessageBox.Show(" Le mouvement 0 est non sécurisé");
            Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte(2);
                msgToSend[1] = Convert.ToByte(0);
                msgToSend[2] = Convert.ToByte(0);

                this.com1.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void alarm_DM0Activated(object sender, EventArgs e)
        {
            Mouvement0.BackColor = Color.Red;
            MessageBox.Show(Mouvement0.Name+ " est maintenant sécurisé");
            Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte(2);
                msgToSend[1] = Convert.ToByte(0);
                msgToSend[2] = Convert.ToByte(1);

                this.com1.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void alarm_P8InActivated(object sender, EventArgs e)
        {
            Porte8.BackColor = Color.White;
            MessageBox.Show(" La porte 8 est non sécurisé");
            Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte(1);
                msgToSend[1] = Convert.ToByte(8);
                msgToSend[2] = Convert.ToByte(0);

                this.com1.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void alarm_P8Activated(object sender, EventArgs e)
        {
            Porte8.BackColor = Color.Red;
            MessageBox.Show(" La porte 8 est maintenant sécurisé");
            Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte(1);
                msgToSend[1] = Convert.ToByte(8);
                msgToSend[2] = Convert.ToByte(1);

                this.com1.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void alarm_P7InActivated(object sender, EventArgs e)
        {
            Porte7.BackColor = Color.White;
            MessageBox.Show(" La porte 7 est non sécurisé");
            Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte(1);
                msgToSend[1] = Convert.ToByte(7);
                msgToSend[2] = Convert.ToByte(0);

                this.com1.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void alarm_P7Activated(object sender, EventArgs e)
        {
            Porte7.BackColor = Color.Red;
            MessageBox.Show(" La porte 7 est maintenant sécurisé");
            Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte(1);
                msgToSend[1] = Convert.ToByte(7);
                msgToSend[2] = Convert.ToByte(1);

                this.com1.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void alarm_P6InActivated(object sender, EventArgs e)
        {
            Porte6.BackColor = Color.White;
            MessageBox.Show(" La porte 6 est non sécurisé");
            Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte(1);
                msgToSend[1] = Convert.ToByte(6);
                msgToSend[2] = Convert.ToByte(0);

                this.com1.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void alarm_P6Activated(object sender, EventArgs e)
        {
            Porte6.BackColor = Color.Red;
            MessageBox.Show(" La porte 6 est maintenant sécurisé");
            Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte(1);
                msgToSend[1] = Convert.ToByte(6);
                msgToSend[2] = Convert.ToByte(1);

                this.com1.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void alarm_P5InActivated(object sender, EventArgs e)
        {
            Porte5.BackColor = Color.White;
            MessageBox.Show(" La porte 5 est non sécurisé");
            Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte(1);
                msgToSend[1] = Convert.ToByte(5);
                msgToSend[2] = Convert.ToByte(0);

                this.com1.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void alarm_P5Activated(object sender, EventArgs e)
        {
            Porte5.BackColor = Color.Red;
            MessageBox.Show(" La porte 5 est maintenant sécurisé");
            Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte(1);
                msgToSend[1] = Convert.ToByte(5);
                msgToSend[2] = Convert.ToByte(1);

                this.com1.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void alarm_P4InActivated(object sender, EventArgs e)
        {
            Porte4.BackColor = Color.White;
            MessageBox.Show(" La porte 4 est non sécurisé");
            Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte(1);
                msgToSend[1] = Convert.ToByte(4);
                msgToSend[2] = Convert.ToByte(0);

                this.com1.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void alarm_P4Activated(object sender, EventArgs e)
        {
            Porte4.BackColor = Color.Red;
            MessageBox.Show(" La porte 4 est maintenant sécurisé");
            Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte(1);
                msgToSend[1] = Convert.ToByte(4);
                msgToSend[2] = Convert.ToByte(1);

                this.com1.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void alarm_P3InActivated(object sender, EventArgs e)
        {
            Porte3.BackColor = Color.White;
            MessageBox.Show(" La porte 3 est non sécurisé");
            Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte(1);
                msgToSend[1] = Convert.ToByte(3);
                msgToSend[2] = Convert.ToByte(0);

                this.com1.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void alarm_P3Activated(object sender, EventArgs e)
        {
            Porte3.BackColor = Color.Red;
            MessageBox.Show(" La porte 3 est maintenant sécurisé");
            Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte(1);
                msgToSend[1] = Convert.ToByte(3);
                msgToSend[2] = Convert.ToByte(1);

                this.com1.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void alarm_P2InActivated(object sender, EventArgs e)
        {
            Porte2.BackColor = Color.White;
            MessageBox.Show(" La porte 2 est non sécurisé");
            Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte(1);
                msgToSend[1] = Convert.ToByte(2);
                msgToSend[2] = Convert.ToByte(0);

                this.com1.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void alarm_P2Activated(object sender, EventArgs e)
        {
            Porte2.BackColor = Color.Red;
            MessageBox.Show(" La porte 2 est maintenant sécurisé");
            Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte(1);
                msgToSend[1] = Convert.ToByte(2);
                msgToSend[2] = Convert.ToByte(1);

                this.com1.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void alarm_P1InActivated(object sender, EventArgs e)
        {
            Porte1.BackColor = Color.White;
            MessageBox.Show(" La porte 1 est non sécurisé");
            Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte(1);
                msgToSend[1] = Convert.ToByte(1);
                msgToSend[2] = Convert.ToByte(0);

                this.com1.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void alarm_P1Activated(object sender, EventArgs e)
        {
            Porte1.BackColor = Color.Red;
            MessageBox.Show(" La porte 1 est maintenant sécurisé");
            Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte(1);
                msgToSend[1] = Convert.ToByte(1);
                msgToSend[2] = Convert.ToByte(1);

                this.com1.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void alarm_P0InActivated(object sender, EventArgs e)
        {
            Porte0.BackColor = Color.White;
            MessageBox.Show(" La porte 0 est non sécurisé");
            Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte(1);
                msgToSend[1] = Convert.ToByte(0);
                msgToSend[2] = Convert.ToByte(0);

                this.com1.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void alarm_P0Activated(object sender, EventArgs e)
        {
            Porte0.BackColor = Color.Red;
            MessageBox.Show(" La porte 0 est maintenant sécurisé");
            Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte(1);
                msgToSend[1] = Convert.ToByte(0);
                msgToSend[2] = Convert.ToByte(1);

                this.com1.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void alarm_F4InActivated(object sender, EventArgs e)
        {
            Fenetre4.BackColor = Color.White;
            MessageBox.Show(" La fenetre 4 est non sécurisé");
            Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte(0);
                msgToSend[1] = Convert.ToByte(4);
                msgToSend[2] = Convert.ToByte(0);

                this.com1.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void alarm_F4Activated(object sender, EventArgs e)
        {
            Fenetre4.BackColor = Color.Red;
            MessageBox.Show(" La fenetre 4 est maintenant sécurisé");
            Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte(0);
                msgToSend[1] = Convert.ToByte(4);
                msgToSend[2] = Convert.ToByte(1);

                this.com1.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void alarm_F3InActivated(object sender, EventArgs e)
        {
            Fenetre3.BackColor = Color.White;
            MessageBox.Show(" La fenetre 3 est non sécurisé");
            Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte(0);
                msgToSend[1] = Convert.ToByte(3);
                msgToSend[2] = Convert.ToByte(0);

                this.com1.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void alarm_F3Activated(object sender, EventArgs e)
        {
            Fenetre3.BackColor = Color.Red;
            MessageBox.Show(" La fenetre 3 est maintenant sécurisé");
            Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte(0);
                msgToSend[1] = Convert.ToByte(3);
                msgToSend[2] = Convert.ToByte(1);

                this.com1.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void alarm_F2InActivated(object sender, EventArgs e)
        {
            Fenetre2.BackColor = Color.White;
            MessageBox.Show(" La fenetre 2 est non sécurisé");
            Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte(0);
                msgToSend[1] = Convert.ToByte(2);
                msgToSend[2] = Convert.ToByte(0);

                this.com1.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void alarm_F2Activated(object sender, EventArgs e)
        {
            Fenetre2.BackColor = Color.Red;
            MessageBox.Show(" La fenetre 2 est maintenant sécurisé");
            Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte(0);
                msgToSend[1] = Convert.ToByte(2);
                msgToSend[2] = Convert.ToByte(1);

                this.com1.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void alarm_F1InActivated(object sender, EventArgs e)
        {
            Fenetre1.BackColor = Color.White;
            MessageBox.Show(" La fenetre 1 est non sécurisé");
             Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte(0);
                msgToSend[1] = Convert.ToByte(1);
                msgToSend[2] = Convert.ToByte(0);

                this.com1.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void alarm_F1Activated(object sender, EventArgs e)
        {
            Fenetre1.BackColor = Color.Red;
            MessageBox.Show(" La fenetre 1 est maintenant sécurisé");
            Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte(0);
                msgToSend[1] = Convert.ToByte(1);
                msgToSend[2] = Convert.ToByte(1);

                this.com1.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void alarm_F0InActivated(object sender, EventArgs e)
        {
            Fenetre0.BackColor = Color.White;
            MessageBox.Show(" La fenetre 0 est non sécurisé");
            Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte(0);
                msgToSend[1] = Convert.ToByte(0);
                msgToSend[2] = Convert.ToByte(0);

                this.com1.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void alarm_F0Activated(object sender, EventArgs e)
        {
            Fenetre0.BackColor = Color.Red;
            Byte[] msgToSend = new Byte[3];
            try
            {
                msgToSend[0] = Convert.ToByte(0);
                msgToSend[1] = Convert.ToByte(0);
                msgToSend[2] = Convert.ToByte(1);

                this.com1.Write(msgToSend, 0, 3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void alarm_SysInActivated(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void alarm_SysActivated(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void Fenetre0_Click(object sender, EventArgs e)
        {
            if (security.Fenetre0)
            {
                security.Fenetre0 = false;
            }
            else
            {
                security.Fenetre0 = true;
            }

        }

        private void Fenetre1_Click(object sender, EventArgs e)
        {
            if (security.Fenetre1)
            {
                security.Fenetre1 = false;
            }
            else
            {
                security.Fenetre1 = true;
            }
        }

        private void Fenetre2_Click(object sender, EventArgs e)
        {
            if (security.Fenetre2)
            {
                security.Fenetre2 = false;
            }
            else
            {
                security.Fenetre2 = true;
            }
        }

        private void Fenetre3_Click(object sender, EventArgs e)
        {
            if (security.Fenetre3)
            {
                security.Fenetre3 = false;
            }
            else
            {
                security.Fenetre3 = true;
            }
        }

        private void Fenetre4_Click(object sender, EventArgs e)
        {
            if (security.Fenetre4)
            {
                security.Fenetre4 = false;
            }
            else
            {
                security.Fenetre4 = true;
            }
        }

        private void Porte0_Click(object sender, EventArgs e)
        {
            if (security.Porte0)
            {
                security.Porte0 = false;
            }
            else
            {
                security.Porte0 = true;
            }
        }

        private void Porte1_Click(object sender, EventArgs e)
        {
            if (security.Porte1)
            {
                security.Porte1 = false;
            }
            else
            {
                security.Porte1 = true;
            }
        }

        private void Porte2_Click(object sender, EventArgs e)
        {
            if (security.Porte2)
            {
                security.Porte2 = false;
            }
            else
            {
                security.Porte2 = true;
            }
        }

        private void Porte3_Click(object sender, EventArgs e)
        {
            if (security.Porte3)
            {
                security.Porte3 = false;
            }
            else
            {
                security.Porte3 = true;
            }
        }

        private void Porte4_Click(object sender, EventArgs e)
        {
            if (security.Porte4)
            {
                security.Porte4 = false;
            }
            else
            {
                security.Porte4 = true;
            }
        }

        private void Porte5_Click(object sender, EventArgs e)
        {
            if (security.Porte5)
            {
                security.Porte5 = false;
            }
            else
            {
                security.Porte5 = true;
            }
        }

        private void Porte6_Click(object sender, EventArgs e)
        {
            if (security.Porte6)
            {
                security.Porte6 = false;
            }
            else
            {
                security.Porte6 = true;
            }
        }

        private void Porte7_Click(object sender, EventArgs e)
        {
            if (security.Porte7)
            {
                security.Porte7 = false;
            }
            else
            {
                security.Porte7 = true;
            }
        }

        private void Porte8_Click(object sender, EventArgs e)
        {
            if (security.Porte8)
            {
                security.Porte8 = false;
            }
            else
            {
                security.Porte8 = true;
            }
        }

        private void Mouvement0_Click(object sender, EventArgs e)
        {
            if (security.Mouvement0)
            {
                security.Mouvement0 = false;
            }
            else
            {
                security.Mouvement0 = true;
            }
        }

        private void Mouvement1_Click(object sender, EventArgs e)
        {
            if (security.Mouvement1)
            {
                security.Mouvement1 = false;
            }
            else
            {
                security.Mouvement1 = true;
            }
        }

        private void Incendie0_Click(object sender, EventArgs e)
        {
            if (security.Incendie0)
            {
                security.Incendie0 = false;
            }
            else
            {
                security.Incendie0 = true;
            }
        }

        private void Incendie1_Click(object sender, EventArgs e)
        {
            if (security.Incendie1)
            {
                security.Incendie1 = false;
            }
            else
            {
                security.Incendie1 = true;
            }
        }

        private void Incendie2_Click(object sender, EventArgs e)
        {
            if (security.Incendie2)
            {
                security.Incendie2 = false;
            }
            else
            {
                security.Incendie2 = true;
            }
        }

        private void frmMaison_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.com1.IsOpen)
            {
                this.com1.Close();
          
                MessageBox.Show("COM1 est maintenant ferme");
            }
        }

        private void MnuChoixCOM_Click(object sender, EventArgs e)
        {
            frmChoixCom choix = new frmChoixCom();
            choix.Show();
        }

        private void MnuQuitter_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Êtes vous sur de vouloir fermer cette application ? ", "Fermeture du programme", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)

            {
                Application.Exit();
            }
        }
    }
}
