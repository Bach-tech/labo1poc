﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PrjPOCLab1Maison
{
    public partial class frmChoixCom : Form
    {
        private SerialPort serialPort1;
        public frmChoixCom()
        {
            InitializeComponent();
        }

        private void cbPortName_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void btnAffiche_Click(object sender, EventArgs e)
        {
            string[] listOfPort = SerialPort.GetPortNames();
            lstCOM.Items.AddRange(listOfPort);
            cbPortName.Items.AddRange(listOfPort);

        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            try
            {
                serialPort1.PortName = cbPortName.Text;
                serialPort1.BaudRate = Convert.ToInt32(cbBaudRate.Text);
                serialPort1.Parity = (Parity)Enum.Parse(typeof(Parity), cbParity.Text);
                serialPort1.StopBits = (StopBits)Enum.Parse(typeof(StopBits), cbStopBits.Text);
                serialPort1.DataBits = Convert.ToInt32(cbDataBits.Text);
                //serialPort1.Handshake = cbHandShake.Text;

                serialPort1.Open();
                MessageBox.Show("Ouverture d'un SerialPort", "Le port " + cbPortName.Text + "est ouvert", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur", ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            lstCOM.Items.Clear();
        }

        private void cbParity_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
