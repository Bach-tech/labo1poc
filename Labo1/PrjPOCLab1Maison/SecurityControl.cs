﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrjPOCLab1Maison
{
    class SecurityControl
    {
        //Déclaration de variables
        private bool F0, F1, F2, F3, F4;
        private bool P0, P1, P2, P3, P4, P5, P6, P7, P8;
        private bool DM0, DM1;
        private bool DI0, DI1, DI2;
        private static int cnt;

        //Etape de la Création d'un événement

        //Etape1:Creation d'un delegate
        public delegate void ControlDelegate(object sender, EventArgs e);

        //Etape2 : Creation des événements pour transmettre aux systemes le statut de notre zone de sécurité

        //Evénement qui nous avertit lorque le système est activé et/ou désactivé
        public event ControlDelegate SysActivated;
        public event ControlDelegate SysInActivated;

        //Evénement qui nous avertit lorque la fenetre 0 est activé et/ou désactivé
        public event ControlDelegate F0Activated;
        public event ControlDelegate F0InActivated;

        //Evénement qui nous avertit lorque la fenetre 1 est activé et/ou désactivé
        public event ControlDelegate F1Activated;
        public event ControlDelegate F1InActivated;

        //Evénement qui nous avertit lorque la fenetre 2 est activé et/ou désactivé
        public event ControlDelegate F2Activated;
        public event ControlDelegate F2InActivated;

        //Evénement qui nous avertit lorque la fenetre 3 est activé et/ou désactivé
        public event ControlDelegate F3Activated;
        public event ControlDelegate F3InActivated;

        //Evénement qui nous avertit lorque la fenetre 4 est activé et/ou désactivé
        public event ControlDelegate F4Activated;
        public event ControlDelegate F4InActivated;

        //Evénement qui nous avertit lorque la porte 0 est activé et/ou désactivé
        public event ControlDelegate P0Activated;
        public event ControlDelegate P0InActivated;

        //Evénement qui nous avertit lorque la porte 1 est activé et/ou désactivé
        public event ControlDelegate P1Activated;
        public event ControlDelegate P1InActivated;

        //Evénement qui nous avertit lorque la porte 2 est activé et/ou désactivé
        public event ControlDelegate P2Activated;
        public event ControlDelegate P2InActivated;

        //Evénement qui nous avertit lorque la porte 3 est activé et/ou désactivé
        public event ControlDelegate P3Activated;
        public event ControlDelegate P3InActivated;

        //Evénement qui nous avertit lorque la porte 4 est activé et/ou désactivé
        public event ControlDelegate P4Activated;
        public event ControlDelegate P4InActivated;

        //Evénement qui nous avertit lorque la porte 5 est activé et/ou désactivé
        public event ControlDelegate P5Activated;
        public event ControlDelegate P5InActivated;

        //Evénement qui nous avertit lorquela porte 6 est activé et/ou désactivé
        public event ControlDelegate P6Activated;
        public event ControlDelegate P6InActivated;

        //Evénement qui nous avertit lorque la porte 7 est activé et/ou désactivé
        public event ControlDelegate P7Activated;
        public event ControlDelegate P7InActivated;

        //Evénement qui nous avertit lorque la porte 8 est activé et/ou désactivé
        public event ControlDelegate P8Activated;
        public event ControlDelegate P8InActivated;

        //Evénement qui nous avertit lorque le mouvement 0 est activé et/ou désactivé
        public event ControlDelegate DM0Activated;
        public event ControlDelegate DM0InActivated;

        //Evénement qui nous avertit lorque le mouvement 1 est activé et/ou désactivé
        public event ControlDelegate DM1Activated;
        public event ControlDelegate DM1InActivated;

        //Evénement qui nous avertit lorque l'incendie 0 est activé et/ou désactivé
        public event ControlDelegate DI0Activated;
        public event ControlDelegate DI0InActivated;

        //Evénement qui nous avertit lorque l'incendie 1 est activé et/ou désactivé
        public event ControlDelegate DI1Activated;
        public event ControlDelegate DI1InActivated;

        //Evénement qui nous avertit lorque l'incendie 2 est activé et/ou désactivé
        public event ControlDelegate DI2Activated;
        public event ControlDelegate DI2InActivated;



        //Etape 3 :Creation d'un constructeur nous permet d'initialiser notre systeme de sécurité
        //Constructeur1 :permet d'incérmenter le nombre d'instances de systemes d'alarmes dans notre système
        // public SystemAlarm()
        // {
        //    cnt++;
        // }
        //Étape4:Création de fonctions permettant de communiquer avec les événements

        //Fonction 1:Permettre d'activer le système de sécurité et lancer l'événement SyActivated
        public void ActivateSystem()
        {
            //Activation du système
            this.F0 = true;
            this.F1 = true;
            this.F2 = true;
            this.F3 = true;
            this.F4 = true;
            this.P0 = true;
            this.P1 = true;
            this.P2 = true;
            this.P3 = true;
            this.P4 = true;
            this.P5 = true;
            this.P6 = true;
            this.P7 = true;
            this.P8 = true;
            this.DM0 = true;
            this.DM1 = true;
            this.DI0 = true;
            this.DI1 = true;
            this.DI2 = true;

            //Lancer l'évenement stipule que le système est activé
            if (SysActivated != null)
            {
                SysActivated(this, new EventArgs());
            }
        }
        //Fonction 2:Permettre de désactiver le système de sécurité et lancer l'événement SyActivated
        public void InactivateSystem()
        {
            //Désactivation du système
            this.F0 = false;
            this.F1 = false;
            this.F2 = false;
            this.F3 = false;
            this.F4 = false;
            this.P0 = false;
            this.P1 = false;
            this.P2 = false;
            this.P3 = false;
            this.P4 = false;
            this.P5 = false;
            this.P6 = false;
            this.P7 = false;
            this.P8 = false;
            this.DM0 = false;
            this.DM1 = false;
            this.DI0 = false;
            this.DI1 = false;
            this.DI2 = false;

            //Lancer l'évenement stipule que le système est désactivé
            if (SysInActivated != null)
            {
                SysInActivated(this, new EventArgs());
            }
        }

        //Etape5 :Création de propriétés pour changer l'état d'une zone puisque les zones sont privées

        //FENETRE 0

        public bool Fenetre0
        {
            get
            {
                return this.F0;
            }
            set
            {
                if (value == true)
                {
                    if (F0Activated != null)
                    {
                        F0Activated(this, new EventArgs());
                    }
                }
                else
                {
                    if (F0InActivated != null)
                    {
                        F0InActivated(this, new EventArgs());
                    }

                }
                this.F0 = value;

            }
        }

        //FENETRE 1

        public bool Fenetre1
        {
            get
            {
                return this.F1;
            }
            set
            {
                if (value == true)
                {
                    if (F1Activated != null)
                    {
                        F1Activated(this, new EventArgs());
                    }
                }
                else
                {
                    if (F1InActivated != null)
                    {
                        F1InActivated(this, new EventArgs());
                    }

                }
                this.F1 = value;

            }
        }

        //FENETRE 2

        public bool Fenetre2
        {
            get
            {
                return this.F2;
            }
            set
            {
                if (value == true)
                {
                    if (F2Activated != null)
                    {
                        F2Activated(this, new EventArgs());
                    }
                }
                else
                {
                    if (F2InActivated != null)
                    {
                        F2InActivated(this, new EventArgs());
                    }

                }
                this.F2 = value;

            }
        }

        //FENETRE 3

        public bool Fenetre3
        {
            get
            {
                return this.F3;
            }
            set
            {
                if (value == true)
                {
                    if (F3Activated != null)
                    {
                        F3Activated(this, new EventArgs());
                    }
                }
                else
                {
                    if (F3InActivated != null)
                    {
                        F3InActivated(this, new EventArgs());
                    }

                }
                this.F3 = value;

            }
        }

        //FENETRE 4

        public bool Fenetre4
        {
            get
            {
                return this.F4;
            }
            set
            {
                if (value == true)
                {
                    if (F4Activated != null)
                    {
                        F4Activated(this, new EventArgs());
                    }
                }
                else
                {
                    if (F4InActivated != null)
                    {
                        F4InActivated(this, new EventArgs());
                    }

                }
                this.F4 = value;

            }
        }

        //PORTE 0

        public bool Porte0
        {
            get
            {
                return this.P0;
            }
            set
            {
                if (value == true)
                {
                    if (P0Activated != null)
                    {
                        P0Activated(this, new EventArgs());
                    }
                }
                else
                {
                    if (P0InActivated != null)
                    {
                        P0InActivated(this, new EventArgs());
                    }

                }
                this.P0 = value;

            }
        }

        //PORTE 1

        public bool Porte1
        {
            get
            {
                return this.P1;
            }
            set
            {
                if (value == true)
                {
                    if (P1Activated != null)
                    {
                        P1Activated(this, new EventArgs());
                    }
                }
                else
                {
                    if (P1InActivated != null)
                    {
                        P1InActivated(this, new EventArgs());
                    }

                }
                this.P1 = value;

            }
        }

        //PORTE 2

        public bool Porte2
        {
            get
            {
                return this.P2;
            }
            set
            {
                if (value == true)
                {
                    if (P2Activated != null)
                    {
                        P2Activated(this, new EventArgs());
                    }
                }
                else
                {
                    if (P2InActivated != null)
                    {
                        P2InActivated(this, new EventArgs());
                    }

                }
                this.P2 = value;

            }
        }

        //PORTE 3

        public bool Porte3
        {
            get
            {
                return this.P3;
            }
            set
            {
                if (value == true)
                {
                    if (P3Activated != null)
                    {
                        P3Activated(this, new EventArgs());
                    }
                }
                else
                {
                    if (P3InActivated != null)
                    {
                        P3InActivated(this, new EventArgs());
                    }

                }
                this.P3 = value;

            }
        }

        //PORTE 4

        public bool Porte4
        {
            get
            {
                return this.P4;
            }
            set
            {
                if (value == true)
                {
                    if (P4Activated != null)
                    {
                        P4Activated(this, new EventArgs());
                    }
                }
                else
                {
                    if (P4InActivated != null)
                    {
                        P4InActivated(this, new EventArgs());
                    }

                }
                this.P4 = value;

            }
        }

        //PORTE 5

        public bool Porte5
        {
            get
            {
                return this.P5;
            }
            set
            {
                if (value == true)
                {
                    if (P5Activated != null)
                    {
                        P5Activated(this, new EventArgs());
                    }
                }
                else
                {
                    if (P5InActivated != null)
                    {
                        P5InActivated(this, new EventArgs());
                    }

                }
                this.P5 = value;

            }
        }

        //PORTE 6

        public bool Porte6
        {
            get
            {
                return this.P6;
            }
            set
            {
                if (value == true)
                {
                    if (P6Activated != null)
                    {
                        P6Activated(this, new EventArgs());
                    }
                }
                else
                {
                    if (P6InActivated != null)
                    {
                        P6InActivated(this, new EventArgs());
                    }

                }
                this.P6 = value;

            }
        }

        //PORTE 7

        public bool Porte7
        {
            get
            {
                return this.P7;
            }
            set
            {
                if (value == true)
                {
                    if (P7Activated != null)
                    {
                        P7Activated(this, new EventArgs());
                    }
                }
                else
                {
                    if (P7InActivated != null)
                    {
                        P7InActivated(this, new EventArgs());
                    }

                }
                this.P7 = value;

            }
        }

        //PORTE 8

        public bool Porte8
        {
            get
            {
                return this.P8;
            }
            set
            {
                if (value == true)
                {
                    if (P8Activated != null)
                    {
                        P8Activated(this, new EventArgs());
                    }
                }
                else
                {
                    if (P8InActivated != null)
                    {
                        P8InActivated(this, new EventArgs());
                    }

                }
                this.P8 = value;

            }
        }

        //MOUVEMENT 0

        public bool Mouvement0
        {
            get
            {
                return this.DM0;
            }
            set
            {
                if (value == true)
                {
                    if (DM0Activated != null)
                    {
                        DM0Activated(this, new EventArgs());
                    }
                }
                else
                {
                    if (DM0InActivated != null)
                    {
                        DM0InActivated(this, new EventArgs());
                    }

                }
                this.DM0 = value;

            }
        }

        //MOUVEMENT 1

        public bool Mouvement1
        {
            get
            {
                return this.DM1;
            }
            set
            {
                if (value == true)
                {
                    if (DM1Activated != null)
                    {
                        DM1Activated(this, new EventArgs());
                    }
                }
                else
                {
                    if (DM1InActivated != null)
                    {
                        DM1InActivated(this, new EventArgs());
                    }

                }
                this.DM1 = value;

            }
        }

        //INCENDIE 0

        public bool Incendie0
        {
            get
            {
                return this.DI0;
            }
            set
            {
                if (value == true)
                {
                    if (DI0Activated != null)
                    {
                        DI0Activated(this, new EventArgs());
                    }
                }
                else
                {
                    if (DI0InActivated != null)
                    {
                        DI0InActivated(this, new EventArgs());
                    }

                }
                this.DI0 = value;

            }
        }

        //INCENDIE 1

        public bool Incendie1
        {
            get
            {
                return this.DI1;
            }
            set
            {
                if (value == true)
                {
                    if (DI1Activated != null)
                    {
                        DI1Activated(this, new EventArgs());
                    }
                }
                else
                {
                    if (DI1InActivated != null)
                    {
                        DI1InActivated(this, new EventArgs());
                    }

                }
                this.DI1 = value;

            }
        }

        //INCENDIE 2

        public bool Incendie2
        {
            get
            {
                return this.DI2;
            }
            set
            {
                if (value == true)
                {
                    if (DI2Activated != null)
                    {
                        DI2Activated(this, new EventArgs());
                    }
                }
                else
                {
                    if (DI2InActivated != null)
                    {
                        DI2InActivated(this, new EventArgs());
                    }

                }
                this.DI2 = value;

            }
        }

    }
}
