﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PrjPOCLab1ControleSecurite
{
    public partial class frmControleSecurite : Form
    {
        private SerialPort com2;
        private delegate void msgDelegate(byte[] b);
        private msgDelegate showReceivedMessage;
        private string message;
        private int type;
        private int numero;
        private int statut;
        private SqlConnection con;
        private SqlCommand cmd;
        private frmHistorique h;


        public frmControleSecurite()
        {
            InitializeComponent();
            this.con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Maussa\Desktop\POC\Laboratoires\Labo1\PrjPOCLab1ControleSecurite\Historique.mdf;Integrated Security=True");
            this.h = new frmHistorique();
        }

        private void toolStripStatusLabel2_Click(object sender, EventArgs e)
        {

        }

        private void btnHistorique_Click(object sender, EventArgs e)
        {
        
            h.Show();
            
        }

        private void frmControleSecurite_Load(object sender, EventArgs e)
        {
            string[] listOfPort = SerialPort.GetPortNames();
            Array.Sort(listOfPort);

            this.com2 = new SerialPort(listOfPort[1], 9600,Parity.None, 8, StopBits.One);
            this.com2.ReceivedBytesThreshold = 3;

            this.com2.DataReceived += Rs232_DataReceived;
             
            try
            {
                this.com2.Open();
                //Insertion de données
                this.message = DateTime.Now + " Ouverture du COM2";
                this.cmd = new SqlCommand("insert into hstMessage(Message)values('" + message + "');", this.con);
                this.con.Open();
                this.cmd.ExecuteNonQuery();
                h.affiche_lstMessage();
                this.con.Close();
                StripLibelle.Text = "Connecté sur: COM2";
                StripConnect.BackColor = Color.Green;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Rs232_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            Byte[] msg = new Byte[3];
            for (int i = 0; i < 3; i++)
            {
                msg[i] = Convert.ToByte(this.com2.ReadByte());
            }

            this.showReceivedMessage = new msgDelegate(ShowMsg);
            this.Invoke(showReceivedMessage, msg);
        }

        private void ShowMsg(byte[] b)
        {

            //FENETRE 0

            if (b[0] == 0 && b[1] == 0 && b[2] == 0)
            {
                this.Fenetre0.BackColor = Color.White;
                this.message = DateTime.Now + " La fenetre 0 est non sécurisée";
                MessageBox.Show(message);
            }
            if (b[0] == 0 && b[1] == 0 && b[2] == 1)
            {
                this.Fenetre0.BackColor = Color.Red;
                this.message = DateTime.Now + " La fenetre 0 est maintenant sécurisée";
                MessageBox.Show(message);
            }

            //FENETRE 1

            if (b[0] == 0 && b[1] == 1 && b[2] == 0)
            {
                this.Fenetre1.BackColor = Color.White;
                this.message = DateTime.Now + " La fenetre 1 est non sécurisée";
                MessageBox.Show(message);
            }
            if (b[0] == 0 && b[1] == 1 && b[2] == 1)
            {
                this.Fenetre1.BackColor = Color.Red;
                this.message = DateTime.Now + " La fenetre 1 est maintenant sécurisée";
                MessageBox.Show(message);
            }

            //FENETRE 2

            if (b[0] == 0 && b[1] == 2 && b[2] == 0)
            {
                this.Fenetre2.BackColor = Color.White;
                this.message = DateTime.Now + " La fenetre 2 est non sécurisée";
                MessageBox.Show(message);
            }
            if (b[0] == 0 && b[1] == 2 && b[2] == 1)
            {
                this.Fenetre2.BackColor = Color.Red;
                this.message = DateTime.Now + " La fenetre 2 est maintenant sécurisée";
                MessageBox.Show(message);
            }

            //FENETRE 3

            if (b[0] == 0 && b[1] == 3 && b[2] == 0)
            {
                this.Fenetre3.BackColor = Color.White;
                this.message = DateTime.Now + " La fenetre 3 est non sécurisée";
                MessageBox.Show(message);
            }
            if (b[0] == 0 && b[1] == 3 && b[2] == 1)
            {
                this.Fenetre3.BackColor = Color.Red;
                this.message = DateTime.Now + " La fenetre 3 est maintenant sécurisée";
                MessageBox.Show(message);
            }

            //FENETRE 4

            if (b[0] == 0 && b[1] == 4 && b[2] == 0)
            {
                this.Fenetre4.BackColor = Color.White;
                this.message = DateTime.Now + " La fenetre 4 est non sécurisée";
                MessageBox.Show(message);
            }
            if (b[0] == 0 && b[1] == 4 && b[2] == 1)
            {
                this.Fenetre4.BackColor = Color.Red;
                this.message = DateTime.Now + " La fenetre 4 est maintenant sécurisée";
                MessageBox.Show(message);
            }

            //PORTE 0

            if (b[0] == 1 && b[1] == 0 && b[2] == 0)
            {
                this.Porte0.BackColor = Color.White;
                this.message = DateTime.Now + " La porte 0 est non sécurisée";
                MessageBox.Show(message);
            }
            if (b[0] == 1 && b[1] == 0 && b[2] == 1)
            {
                this.Porte0.BackColor = Color.Red;
                this.message = DateTime.Now + " La porte 0 est maintenant sécurisée";
                MessageBox.Show(message);
            }

            //PORTE 1

            if (b[0] == 1 && b[1] == 1 && b[2] == 0)
            {
                this.Porte1.BackColor = Color.White;
                this.message = DateTime.Now + " La porte 1 est non sécurisée";
                MessageBox.Show(message);
            }
            if (b[0] == 1 && b[1] == 1 && b[2] == 1)
            {
                this.Porte1.BackColor = Color.Red;
                this.message = DateTime.Now + " La porte 1 est maintenant sécurisée";
                MessageBox.Show(message);
            }

            //PORTE 2

            if (b[0] == 1 && b[1] == 2 && b[2] == 0)
            {
                this.Porte2.BackColor = Color.White;
                this.message = DateTime.Now + " La porte 2 est non sécurisée";
                MessageBox.Show(message);
            }
            if (b[0] == 1 && b[1] == 2 && b[2] == 1)
            {
                this.Porte2.BackColor = Color.Red;
                this.message = DateTime.Now + " La porte 2 est maintenant sécurisée";
                MessageBox.Show(message);
            }

            //PORTE 3

            if (b[0] == 1 && b[1] == 3 && b[2] == 0)
            {
                this.Porte3.BackColor = Color.White;
                this.message = DateTime.Now + " La porte 3est non sécurisée";
                MessageBox.Show(message);
            }
            if (b[0] == 1 && b[1] == 3 && b[2] == 1)
            {
                this.Porte3.BackColor = Color.Red;
                this.message = DateTime.Now + " La porte 3 est maintenant sécurisée";
                MessageBox.Show(message);
            }

            //PORTE 4

            if (b[0] == 1 && b[1] == 4 && b[2] == 0)
            {
                this.Porte4.BackColor = Color.White;
                this.message = DateTime.Now + " La porte 4 est non sécurisée";
                MessageBox.Show(message);
            }
            if (b[0] == 1 && b[1] == 4 && b[2] == 1)
            {
                this.Porte4.BackColor = Color.Red;
                this.message = DateTime.Now + " La porte 4 est maintenant sécurisée";
                MessageBox.Show(message);
            }

            //PORTE 5

            if (b[0] == 1 && b[1] == 5 && b[2] == 0)
            {
                this.Porte5.BackColor = Color.White;
                this.message = DateTime.Now + " La porte 5 est non sécurisée";
                MessageBox.Show(message);
            }
            if (b[0] == 1 && b[1] == 5 && b[2] == 1)
            {
                this.Porte5.BackColor = Color.Red;
                this.message = DateTime.Now + " La porte 5 est maintenant sécurisée";
                MessageBox.Show(message);
            }

            //PORTE 6

            if (b[0] == 1 && b[1] == 6 && b[2] == 0)
            {
                this.Porte6.BackColor = Color.White;
                this.message = DateTime.Now + " La porte 6 est non sécurisée";
                MessageBox.Show(message);
            }
            if (b[0] == 1 && b[1] == 6 && b[2] == 1)
            {
                this.Porte6.BackColor = Color.Red;
                this.message = DateTime.Now + " La porte 6 est maintenant sécurisée";
                MessageBox.Show(message);
            }

            //PORTE 7

            if (b[0] == 1 && b[1] == 7 && b[2] == 0)
            {
                this.Porte7.BackColor = Color.White;
                this.message = DateTime.Now + " La porte 7 est non sécurisée";
                MessageBox.Show(message);
            }
            if (b[0] == 1 && b[1] == 7 && b[2] == 1)
            {
                this.Porte7.BackColor = Color.Red;
                this.message = DateTime.Now + " La porte 7 est maintenant sécurisée";
                MessageBox.Show(message);
            }


            //PORTE 8

            if (b[0] == 1 && b[1] == 8 && b[2] == 0)
            {
                this.Porte8.BackColor = Color.White;
                this.message = DateTime.Now + " La porte 8 est non sécurisée";
                MessageBox.Show(message);
            }
            if (b[0] == 1 && b[1] == 8 && b[2] == 1)
            {
                this.Porte8.BackColor = Color.Red;
                this.message = DateTime.Now + " La porte 8 est maintenant sécurisée";
                MessageBox.Show(message);
            }

            //Mouvement 0

            if (b[0] == 2 && b[1] == 0 && b[2] == 0)
            {
                this.Mouvement0.BackColor = Color.White;
                this.message = DateTime.Now + " Le mouvement 0 est non sécurisée";
                MessageBox.Show(message);
            }
            if (b[0] == 2 && b[1] == 0 && b[2] == 1)
            {
                this.Mouvement0.BackColor = Color.Red;
                this.message = DateTime.Now + " Le mouvement 0 est maintenant sécurisée";
                MessageBox.Show(message);
            }

            //Mouvement 1

            if (b[0] == 2 && b[1] == 1 && b[2] == 0)
            {
                this.Mouvement1.BackColor = Color.White;
                this.message = DateTime.Now + " Le mouvement 1 est non sécurisée";
                MessageBox.Show(message);
            }
            if (b[0] == 2 && b[1] == 1 && b[2] == 1)
            {
                this.Mouvement1.BackColor = Color.Red;
                this.message = DateTime.Now + " Le mouvement 1 est maintenant sécurisée";
                MessageBox.Show(message);
            }

            //Incendie 0

            if (b[0] == 3 && b[1] == 0 && b[2] == 0)
            {
                this.Incendie0.BackColor = Color.White;
                this.message = DateTime.Now + " Incendie 0 est non sécurisée";
                MessageBox.Show(message);
            }
            if (b[0] == 3 && b[1] == 0 && b[2] == 1)
            {
                this.Incendie0.BackColor = Color.Red;
                this.message = DateTime.Now + " Incendie 0 est maintenant sécurisée";
                MessageBox.Show(message);
            }

            //Incendie 1

            if (b[0] == 3 && b[1] == 1 && b[2] == 0)
            {
                this.Incendie1.BackColor = Color.White;
                this.message = DateTime.Now + " Incendie 1 est non sécurisée";
                MessageBox.Show(message);
            }
            if (b[0] == 3 && b[1] == 1 && b[2] == 1)
            {
                this.Incendie1.BackColor = Color.Red;
                this.message = DateTime.Now + " Incendie 1 est maintenant sécurisée";
                MessageBox.Show(message);
            }

            //Incendie 2

            if (b[0] == 3 && b[1] == 2 && b[2] == 0)
            {
                this.Incendie2.BackColor = Color.White;
                this.message = DateTime.Now + " Incendie 2 est non sécurisée";
                MessageBox.Show(message);
            }
            if (b[0] == 3 && b[1] == 2 && b[2] == 1)
            {
                this.Incendie2.BackColor = Color.Red;
                this.message = DateTime.Now + " Incendie 2 est maintenant sécurisée";
                MessageBox.Show(message);
            }

            this.type = b[0];
            this.numero =b[1];
            this.statut = b[2];
            //Insertion de données
            this.cmd = new SqlCommand("insert into hstMessage(Type,Numero,Statut,Message)values("+ type + "," + numero + "," + statut + ",'" + message + "');", this.con);
            this.con.Open();
            this.cmd.ExecuteNonQuery();
            h.affiche_lstMessage();
            this.con.Close();

        }


       

        private void MnuChoixDeCOM_Click(object sender, EventArgs e)
        {
            frmChoixCom choix = new frmChoixCom();
            choix.Show();
        }

        private void MnuQuitter_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Êtes vous sur de vouloir fermer cette application ? ", "Fermeture du programme", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)

            {
                Application.Exit();
            }
        }

        private void frmControleSecurite_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Insertion de données
            this.message = DateTime.Now + " Fermeture du COM2";
            this.cmd = new SqlCommand("insert into hstMessage(Message)values('" + message + "');", this.con);
            this.con.Open();
            this.cmd.ExecuteNonQuery();
            h.affiche_lstMessage();
            this.con.Close();
        }
    }
}
