﻿
namespace PrjPOCLab1ControleSecurite
{
    partial class frmControleSecurite
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmControleSecurite));
            this.Incendie0 = new System.Windows.Forms.Button();
            this.Incendie1 = new System.Windows.Forms.Button();
            this.Incendie2 = new System.Windows.Forms.Button();
            this.Mouvement0 = new System.Windows.Forms.Button();
            this.Mouvement1 = new System.Windows.Forms.Button();
            this.Porte0 = new System.Windows.Forms.Button();
            this.Porte1 = new System.Windows.Forms.Button();
            this.Porte2 = new System.Windows.Forms.Button();
            this.Porte3 = new System.Windows.Forms.Button();
            this.Porte4 = new System.Windows.Forms.Button();
            this.Porte5 = new System.Windows.Forms.Button();
            this.Porte6 = new System.Windows.Forms.Button();
            this.Porte7 = new System.Windows.Forms.Button();
            this.Porte8 = new System.Windows.Forms.Button();
            this.Fenetre1 = new System.Windows.Forms.Button();
            this.Fenetre2 = new System.Windows.Forms.Button();
            this.Fenetre4 = new System.Windows.Forms.Button();
            this.Fenetre3 = new System.Windows.Forms.Button();
            this.Fenetre0 = new System.Windows.Forms.Button();
            this.Plan_Maison = new System.Windows.Forms.PictureBox();
            this.btnHistorique = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.StripLibelle = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.StripConnect = new System.Windows.Forms.ToolStripStatusLabel();
            this.footer = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fichierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MnuChoixDeCOM = new System.Windows.Forms.ToolStripMenuItem();
            this.MnuQuitter = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.Plan_Maison)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Incendie0
            // 
            this.Incendie0.BackColor = System.Drawing.Color.White;
            this.Incendie0.Location = new System.Drawing.Point(11, 168);
            this.Incendie0.Name = "Incendie0";
            this.Incendie0.Size = new System.Drawing.Size(64, 61);
            this.Incendie0.TabIndex = 42;
            this.Incendie0.Text = "DI0";
            this.Incendie0.UseVisualStyleBackColor = false;
            // 
            // Incendie1
            // 
            this.Incendie1.BackColor = System.Drawing.Color.White;
            this.Incendie1.Location = new System.Drawing.Point(283, 168);
            this.Incendie1.Name = "Incendie1";
            this.Incendie1.Size = new System.Drawing.Size(65, 38);
            this.Incendie1.TabIndex = 41;
            this.Incendie1.Text = "DI1";
            this.Incendie1.UseVisualStyleBackColor = false;
            // 
            // Incendie2
            // 
            this.Incendie2.BackColor = System.Drawing.Color.White;
            this.Incendie2.Location = new System.Drawing.Point(438, 427);
            this.Incendie2.Name = "Incendie2";
            this.Incendie2.Size = new System.Drawing.Size(51, 39);
            this.Incendie2.TabIndex = 40;
            this.Incendie2.Text = "DI2";
            this.Incendie2.UseVisualStyleBackColor = false;
            // 
            // Mouvement0
            // 
            this.Mouvement0.BackColor = System.Drawing.Color.White;
            this.Mouvement0.Location = new System.Drawing.Point(63, 307);
            this.Mouvement0.Name = "Mouvement0";
            this.Mouvement0.Size = new System.Drawing.Size(95, 51);
            this.Mouvement0.TabIndex = 39;
            this.Mouvement0.Text = "DM0";
            this.Mouvement0.UseVisualStyleBackColor = false;
            // 
            // Mouvement1
            // 
            this.Mouvement1.BackColor = System.Drawing.Color.White;
            this.Mouvement1.Location = new System.Drawing.Point(505, 338);
            this.Mouvement1.Name = "Mouvement1";
            this.Mouvement1.Size = new System.Drawing.Size(63, 51);
            this.Mouvement1.TabIndex = 38;
            this.Mouvement1.Text = "DM1";
            this.Mouvement1.UseVisualStyleBackColor = false;
            // 
            // Porte0
            // 
            this.Porte0.BackColor = System.Drawing.Color.White;
            this.Porte0.Location = new System.Drawing.Point(393, 336);
            this.Porte0.Name = "Porte0";
            this.Porte0.Size = new System.Drawing.Size(47, 51);
            this.Porte0.TabIndex = 37;
            this.Porte0.Text = "P0";
            this.Porte0.UseVisualStyleBackColor = false;
            // 
            // Porte1
            // 
            this.Porte1.BackColor = System.Drawing.Color.White;
            this.Porte1.Location = new System.Drawing.Point(366, 247);
            this.Porte1.Name = "Porte1";
            this.Porte1.Size = new System.Drawing.Size(49, 51);
            this.Porte1.TabIndex = 36;
            this.Porte1.Text = "P1";
            this.Porte1.UseVisualStyleBackColor = false;
            // 
            // Porte2
            // 
            this.Porte2.BackColor = System.Drawing.Color.White;
            this.Porte2.Location = new System.Drawing.Point(423, 235);
            this.Porte2.Name = "Porte2";
            this.Porte2.Size = new System.Drawing.Size(47, 51);
            this.Porte2.TabIndex = 35;
            this.Porte2.Text = "P2";
            this.Porte2.UseVisualStyleBackColor = false;
            // 
            // Porte3
            // 
            this.Porte3.BackColor = System.Drawing.Color.White;
            this.Porte3.Location = new System.Drawing.Point(447, 193);
            this.Porte3.Name = "Porte3";
            this.Porte3.Size = new System.Drawing.Size(42, 36);
            this.Porte3.TabIndex = 34;
            this.Porte3.Text = "P3";
            this.Porte3.UseVisualStyleBackColor = false;
            // 
            // Porte4
            // 
            this.Porte4.BackColor = System.Drawing.Color.White;
            this.Porte4.Location = new System.Drawing.Point(423, 158);
            this.Porte4.Name = "Porte4";
            this.Porte4.Size = new System.Drawing.Size(47, 38);
            this.Porte4.TabIndex = 33;
            this.Porte4.Text = "P4";
            this.Porte4.UseVisualStyleBackColor = false;
            // 
            // Porte5
            // 
            this.Porte5.BackColor = System.Drawing.Color.White;
            this.Porte5.Location = new System.Drawing.Point(382, 158);
            this.Porte5.Name = "Porte5";
            this.Porte5.Size = new System.Drawing.Size(42, 38);
            this.Porte5.TabIndex = 32;
            this.Porte5.Text = "P5";
            this.Porte5.UseVisualStyleBackColor = false;
            // 
            // Porte6
            // 
            this.Porte6.BackColor = System.Drawing.Color.White;
            this.Porte6.Location = new System.Drawing.Point(227, 258);
            this.Porte6.Name = "Porte6";
            this.Porte6.Size = new System.Drawing.Size(54, 54);
            this.Porte6.TabIndex = 31;
            this.Porte6.Text = "P6";
            this.Porte6.UseVisualStyleBackColor = false;
            // 
            // Porte7
            // 
            this.Porte7.BackColor = System.Drawing.Color.White;
            this.Porte7.Location = new System.Drawing.Point(438, 501);
            this.Porte7.Name = "Porte7";
            this.Porte7.Size = new System.Drawing.Size(119, 37);
            this.Porte7.TabIndex = 30;
            this.Porte7.Text = "P7";
            this.Porte7.UseVisualStyleBackColor = false;
            // 
            // Porte8
            // 
            this.Porte8.BackColor = System.Drawing.Color.White;
            this.Porte8.Location = new System.Drawing.Point(468, 281);
            this.Porte8.Name = "Porte8";
            this.Porte8.Size = new System.Drawing.Size(52, 51);
            this.Porte8.TabIndex = 29;
            this.Porte8.Text = "P8";
            this.Porte8.UseVisualStyleBackColor = false;
            // 
            // Fenetre1
            // 
            this.Fenetre1.BackColor = System.Drawing.Color.White;
            this.Fenetre1.Location = new System.Drawing.Point(261, 37);
            this.Fenetre1.Name = "Fenetre1";
            this.Fenetre1.Size = new System.Drawing.Size(75, 37);
            this.Fenetre1.TabIndex = 28;
            this.Fenetre1.Text = "F1";
            this.Fenetre1.UseVisualStyleBackColor = false;
            // 
            // Fenetre2
            // 
            this.Fenetre2.BackColor = System.Drawing.Color.White;
            this.Fenetre2.Location = new System.Drawing.Point(452, 37);
            this.Fenetre2.Name = "Fenetre2";
            this.Fenetre2.Size = new System.Drawing.Size(75, 37);
            this.Fenetre2.TabIndex = 27;
            this.Fenetre2.Text = "F2";
            this.Fenetre2.UseVisualStyleBackColor = false;
            // 
            // Fenetre4
            // 
            this.Fenetre4.BackColor = System.Drawing.Color.White;
            this.Fenetre4.Location = new System.Drawing.Point(317, 501);
            this.Fenetre4.Name = "Fenetre4";
            this.Fenetre4.Size = new System.Drawing.Size(85, 37);
            this.Fenetre4.TabIndex = 26;
            this.Fenetre4.Text = "F4";
            this.Fenetre4.UseVisualStyleBackColor = false;
            // 
            // Fenetre3
            // 
            this.Fenetre3.BackColor = System.Drawing.Color.White;
            this.Fenetre3.Location = new System.Drawing.Point(248, 336);
            this.Fenetre3.Name = "Fenetre3";
            this.Fenetre3.Size = new System.Drawing.Size(58, 51);
            this.Fenetre3.TabIndex = 25;
            this.Fenetre3.Text = "F3";
            this.Fenetre3.UseVisualStyleBackColor = false;
            // 
            // Fenetre0
            // 
            this.Fenetre0.BackColor = System.Drawing.Color.White;
            this.Fenetre0.Location = new System.Drawing.Point(83, 37);
            this.Fenetre0.Name = "Fenetre0";
            this.Fenetre0.Size = new System.Drawing.Size(75, 37);
            this.Fenetre0.TabIndex = 24;
            this.Fenetre0.Text = "F0";
            this.Fenetre0.UseVisualStyleBackColor = false;
            // 
            // Plan_Maison
            // 
            this.Plan_Maison.Image = ((System.Drawing.Image)(resources.GetObject("Plan_Maison.Image")));
            this.Plan_Maison.Location = new System.Drawing.Point(0, 36);
            this.Plan_Maison.Name = "Plan_Maison";
            this.Plan_Maison.Size = new System.Drawing.Size(569, 501);
            this.Plan_Maison.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Plan_Maison.TabIndex = 23;
            this.Plan_Maison.TabStop = false;
            // 
            // btnHistorique
            // 
            this.btnHistorique.Location = new System.Drawing.Point(12, 425);
            this.btnHistorique.Name = "btnHistorique";
            this.btnHistorique.Size = new System.Drawing.Size(158, 33);
            this.btnHistorique.TabIndex = 45;
            this.btnHistorique.Text = "Historique";
            this.btnHistorique.UseVisualStyleBackColor = true;
            this.btnHistorique.Click += new System.EventHandler(this.btnHistorique_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StripLibelle,
            this.toolStripStatusLabel3,
            this.StripConnect,
            this.footer});
            this.statusStrip1.Location = new System.Drawing.Point(0, 537);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(569, 32);
            this.statusStrip1.TabIndex = 46;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // StripLibelle
            // 
            this.StripLibelle.Enabled = false;
            this.StripLibelle.Name = "StripLibelle";
            this.StripLibelle.Size = new System.Drawing.Size(0, 25);
            this.StripLibelle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.AutoSize = false;
            this.toolStripStatusLabel3.BackColor = System.Drawing.Color.Black;
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(0, 25);
            // 
            // StripConnect
            // 
            this.StripConnect.AutoSize = false;
            this.StripConnect.BackColor = System.Drawing.Color.Black;
            this.StripConnect.Name = "StripConnect";
            this.StripConnect.Size = new System.Drawing.Size(30, 25);
            // 
            // footer
            // 
            this.footer.Name = "footer";
            this.footer.Size = new System.Drawing.Size(215, 25);
            this.footer.Text = "Contrôle de sécurité 2021";
            this.footer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // menuStrip1
            // 
            this.menuStrip1.GripMargin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fichierToolStripMenuItem,
            this.toolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(569, 33);
            this.menuStrip1.TabIndex = 47;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fichierToolStripMenuItem
            // 
            this.fichierToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MnuChoixDeCOM,
            this.MnuQuitter});
            this.fichierToolStripMenuItem.Name = "fichierToolStripMenuItem";
            this.fichierToolStripMenuItem.Size = new System.Drawing.Size(78, 29);
            this.fichierToolStripMenuItem.Text = "Fichier";
            // 
            // MnuChoixDeCOM
            // 
            this.MnuChoixDeCOM.Name = "MnuChoixDeCOM";
            this.MnuChoixDeCOM.Size = new System.Drawing.Size(229, 34);
            this.MnuChoixDeCOM.Text = "Choix de COM";
            this.MnuChoixDeCOM.Click += new System.EventHandler(this.MnuChoixDeCOM_Click);
            // 
            // MnuQuitter
            // 
            this.MnuQuitter.Name = "MnuQuitter";
            this.MnuQuitter.Size = new System.Drawing.Size(229, 34);
            this.MnuQuitter.Text = "Quitter";
            this.MnuQuitter.Click += new System.EventHandler(this.MnuQuitter_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(36, 29);
            this.toolStripMenuItem1.Text = "?";
            // 
            // frmControleSecurite
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(569, 569);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.btnHistorique);
            this.Controls.Add(this.Incendie0);
            this.Controls.Add(this.Incendie1);
            this.Controls.Add(this.Incendie2);
            this.Controls.Add(this.Mouvement0);
            this.Controls.Add(this.Mouvement1);
            this.Controls.Add(this.Porte0);
            this.Controls.Add(this.Porte1);
            this.Controls.Add(this.Porte2);
            this.Controls.Add(this.Porte3);
            this.Controls.Add(this.Porte4);
            this.Controls.Add(this.Porte5);
            this.Controls.Add(this.Porte6);
            this.Controls.Add(this.Porte7);
            this.Controls.Add(this.Porte8);
            this.Controls.Add(this.Fenetre1);
            this.Controls.Add(this.Fenetre2);
            this.Controls.Add(this.Fenetre4);
            this.Controls.Add(this.Fenetre3);
            this.Controls.Add(this.Fenetre0);
            this.Controls.Add(this.Plan_Maison);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmControleSecurite";
            this.Text = "Controle de sécurité";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmControleSecurite_FormClosing);
            this.Load += new System.EventHandler(this.frmControleSecurite_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Plan_Maison)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button Incendie0;
        private System.Windows.Forms.Button Incendie1;
        private System.Windows.Forms.Button Incendie2;
        private System.Windows.Forms.Button Mouvement0;
        private System.Windows.Forms.Button Mouvement1;
        private System.Windows.Forms.Button Porte0;
        private System.Windows.Forms.Button Porte1;
        private System.Windows.Forms.Button Porte2;
        private System.Windows.Forms.Button Porte3;
        private System.Windows.Forms.Button Porte4;
        private System.Windows.Forms.Button Porte5;
        private System.Windows.Forms.Button Porte6;
        private System.Windows.Forms.Button Porte7;
        private System.Windows.Forms.Button Porte8;
        private System.Windows.Forms.Button Fenetre1;
        private System.Windows.Forms.Button Fenetre2;
        private System.Windows.Forms.Button Fenetre4;
        private System.Windows.Forms.Button Fenetre3;
        private System.Windows.Forms.Button Fenetre0;
        private System.Windows.Forms.PictureBox Plan_Maison;
        private System.Windows.Forms.Button btnHistorique;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel StripLibelle;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripStatusLabel StripConnect;
        private System.Windows.Forms.ToolStripStatusLabel footer;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fichierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem MnuChoixDeCOM;
        private System.Windows.Forms.ToolStripMenuItem MnuQuitter;
    }
}

