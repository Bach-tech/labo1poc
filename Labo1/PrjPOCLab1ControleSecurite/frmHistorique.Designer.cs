﻿
namespace PrjPOCLab1ControleSecurite
{
    partial class frmHistorique
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LstMessage = new System.Windows.Forms.ListBox();
            this.btnEffacer = new System.Windows.Forms.Button();
            this.btnPlan = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // LstMessage
            // 
            this.LstMessage.FormattingEnabled = true;
            this.LstMessage.ItemHeight = 20;
            this.LstMessage.Location = new System.Drawing.Point(1, 1);
            this.LstMessage.Name = "LstMessage";
            this.LstMessage.Size = new System.Drawing.Size(609, 204);
            this.LstMessage.TabIndex = 0;
            this.LstMessage.SelectedIndexChanged += new System.EventHandler(this.LblMessage_SelectedIndexChanged);
            // 
            // btnEffacer
            // 
            this.btnEffacer.Location = new System.Drawing.Point(654, 33);
            this.btnEffacer.Name = "btnEffacer";
            this.btnEffacer.Size = new System.Drawing.Size(116, 39);
            this.btnEffacer.TabIndex = 1;
            this.btnEffacer.Text = "Effacer";
            this.btnEffacer.UseVisualStyleBackColor = true;
            // 
            // btnPlan
            // 
            this.btnPlan.Location = new System.Drawing.Point(654, 111);
            this.btnPlan.Name = "btnPlan";
            this.btnPlan.Size = new System.Drawing.Size(116, 39);
            this.btnPlan.TabIndex = 2;
            this.btnPlan.Text = "Plan";
            this.btnPlan.UseVisualStyleBackColor = true;
            this.btnPlan.Click += new System.EventHandler(this.btnPlan_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(-4, 208);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(400, 29);
            this.label1.TabIndex = 3;
            this.label1.Text = "Fermer la fenêtre pour rétablir l\'onglet historique.";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // frmHistorique
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 243);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnPlan);
            this.Controls.Add(this.btnEffacer);
            this.Controls.Add(this.LstMessage);
            this.Name = "frmHistorique";
            this.Text = "Historique (0)";
            this.Load += new System.EventHandler(this.frmHistorique_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox LstMessage;
        private System.Windows.Forms.Button btnEffacer;
        private System.Windows.Forms.Button btnPlan;
        private System.Windows.Forms.Label label1;
    }
}