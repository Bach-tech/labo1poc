﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace PrjPOCLab1ControleSecurite
{
    public partial class frmHistorique : Form
    {
        private SqlConnection con;
        private SqlCommand cmd;
        private SqlDataReader reader;
        public frmHistorique()
        {
            InitializeComponent();
            this.con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Maussa\Desktop\POC\Laboratoires\Labo1\PrjPOCLab1ControleSecurite\Historique.mdf;Integrated Security=True");

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void LblMessage_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void frmHistorique_Load(object sender, EventArgs e)
        {
            affiche_lstMessage();
        }

        public void affiche_lstMessage()
        {
            //LstMessage.Items.Clear();
            //this.con.Open();
            ////Affichage des messages dans lstMessage
            //this.cmd = new SqlCommand("select Message from hstMessage;", this.con);
            //this.cmd.ExecuteNonQuery();
            //DataTable data = new DataTable();
            //SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            //adapter.Fill(data);
            //foreach (DataRow dr in data.Rows)
            //{
            //    LstMessage.Items.Add(dr["Message"].ToString());
            //}
            //this.con.Close();

            string message;
            LstMessage.Items.Clear();
            this.cmd = new SqlCommand("select Message from hstMessage;", this.con);
            this.con.Open();
            this.reader = this.cmd.ExecuteReader();
            while (reader.Read())
            {
                this.LstMessage.Items.Add(

                    message = reader["Message"].ToString()
                   ); 
            }
            this.con.Close();
            

        }

        private void btnPlan_Click(object sender, EventArgs e)
        {
            frmControleSecurite cs = new frmControleSecurite();
            cs.Show();
        }
    }
}
