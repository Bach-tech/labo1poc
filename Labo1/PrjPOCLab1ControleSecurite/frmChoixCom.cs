﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PrjPOCLab1ControleSecurite
{
    public partial class frmChoixCom : Form
    {
        private SerialPort serialPort2;
        public frmChoixCom()
        {
            InitializeComponent();
        }

        private void frmChoixCom_Load(object sender, EventArgs e)
        {
           
        }

        private void cbBaudRate_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cbStopBits_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnAffiche_Click(object sender, EventArgs e)
        {
            string[] listOfPort = SerialPort.GetPortNames();
            lstCOM.Items.AddRange(listOfPort);
            cbPortName.Items.AddRange(listOfPort);
        }

        private void cbPortName_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            try
            {
                serialPort2.PortName = cbPortName.SelectedItem.ToString();
                serialPort2.BaudRate = Convert.ToInt32(cbBaudRate.SelectedItem);
                serialPort2.Parity = (Parity)Enum.Parse(typeof(Parity), cbParity.SelectedItem.ToString());
                serialPort2.StopBits = (StopBits)Enum.Parse(typeof(StopBits), cbStopBits.SelectedItem.ToString());
                serialPort2.DataBits = Convert.ToInt32(cbDataBits.SelectedItem.ToString());
               

                serialPort2.Open();
                MessageBox.Show("Ouverture d'un SerialPort", "Le port "+cbPortName.Text+"est ouvert" , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch(Exception ex)
            {
                MessageBox.Show("Erreur",ex.Message,MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
           
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            lstCOM.Items.Clear();
        }

        private void cbParity_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
